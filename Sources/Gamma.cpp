#include "Gamma.hpp"
#include "Generator.hpp"

Gamma::Gamma():param_a(-1), param_b(-1) {      //an invalid allocating for trusting
  Generator::Gen();                            //make ready gsl_rng
}

Gamma::~Gamma() {
  
}

void Gamma::set_param_a(double x) {           //Get Gamma needed Parameter (a)
  try {                                       //checking value (a > 0)
    if (x <= 0)
    {
      throw std::out_of_range("Parameter 'a' must be more than 0");
    }
    else
    {
      this->param_a = x; 
    }
  }
  catch(const std::out_of_range &e)
  {
    std::cerr << e.what() << '\n';
  }
}

void Gamma::set_param_b(double x) {           //Get Gamma needed Parameter (b)
  try {                                       //checking value (b > 0)
    if (x <= 0)
    {
      throw std::out_of_range("Parameter 'b' must be more than 0");
    }
    else
    {
      this->param_b = x; 
    }
  }
  catch(const std::out_of_range &e)
  {
    std::cerr << e.what() << '\n';
  }
}

double Gamma::Calculate(const gsl_rng *gr) {    //The main function is to calculate the value
  try                                           //Input reliability check
  {
    if (this->param_a <= 0 || this->param_b <= 0) throw std::bad_alloc();
    return gsl_ran_gamma(gr, this->param_a, this->param_b);
  }
  catch(const std::bad_alloc& e)
  {
    std::cerr << "Your distribution (Gamma Dist)";
    throw;
  }
  
}