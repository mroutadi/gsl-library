#include "Gaussian.hpp"
#include "Generator.hpp"

Gaussian::Gaussian():param_sigma(-1) {         //an invalid allocating for trusting
  Generator::Gen();                            //make ready gsl_rng
}

Gaussian::~Gaussian() {

}

void Gaussian::set_param_sigma(double sigma) {//Get Gaussian needed Parameter (Σ (sigma))
  try {                                       //checking value (Σ > 0)
    if (sigma <= 0)
    {
      throw std::out_of_range("Parameter 'Σ' must be more than 0");
    }
    else
    {
      this->param_sigma = sigma; 
    }
  }
  catch(const std::out_of_range &e)
  {
    std::cerr << e.what() << '\n';
  }
}

double Gaussian::Calculate(const gsl_rng* gr) { //The main function is to calculate the value
  try                                           //Input reliability check
  {
    if (this->param_sigma <= 0) throw std::bad_alloc();
    return gsl_ran_gaussian(gr, this->param_sigma);
  }
  catch(const std::bad_alloc& e)
  {
    std::cerr << "Your distribution‌ (Gaussian Dist)";
    throw;
  }
  
}