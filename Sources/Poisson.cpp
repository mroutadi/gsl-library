#include "Poisson.hpp"
#include "Generator.hpp"

Poisson::Poisson():param_mu(-1) {              //an invalid allocating for trusting
  Generator::Gen();                            //make ready gsl_rng
}

Poisson::~Poisson() {
  
}

void Poisson::set_param_mu(double mu) {       //Get Poisson needed Parameter (μ (mu))
  try {                                       //checking value (μ > 0)
    if (mu <= 0)
    {
      throw std::out_of_range("Parameter 'μ' must be more than 0");
    }
    else
    {
      this->param_mu = mu;
    }
  }
  catch(const std::out_of_range &e)
  {
    std::cerr << e.what() << '\n';
  }
}

double Poisson::Calculate(const gsl_rng* gr) {  //The main function is to calculate the value
  try                                           //Input reliability check
  {
    if (this->param_mu <= 0) throw std::bad_alloc();
    return double(gsl_ran_poisson(gr, this->param_mu));
  }
  catch(const std::bad_alloc& e)
  {
    std::cerr << "Your distribution‌ (Poisson Dist)";
    throw;
  }
}