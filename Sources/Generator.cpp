#include "Generator.hpp"

Generator * Generator::Val = nullptr;       //first static initialize (Val is instannce pointer)
gsl_rng * Generator::r = nullptr;           //we create random once

Generator::Generator() {                    //The work needed to produce the generator is done here
  const gsl_rng_type * T;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  this->r = gsl_rng_alloc(T);
  gsl_rng_set(this->r, time(0));
}

Generator::~Generator() {                   //Generator destructor that make memory free
  gsl_rng_free(this->r);
}

Generator * Generator::Gen() {              //instance Generator generator (Powerd by Singleton Pattern)
  if (Val == nullptr)
  {
    Val = new Generator();
  }
  return Val;
}

RandomStream Generator::create_random_stream (const Distribution* Dist) {   //try to create appropriate Distribute
  return RandomStream(Dist, r);
}