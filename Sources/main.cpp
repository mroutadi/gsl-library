// In The Name Of GOD
// Name : Mohammad Outadi
// ID   : 97 12 35 80 03
/* Some Examples are provide in main function */
#include "Distribution.hpp"
#include "Gamma.hpp"
#include "Gaussian.hpp"
#include "Generator.hpp"
#include "Poisson.hpp"
#include "RandomStream.hpp"

using namespace std;

int main() {
  /* Gamma Distribution */
    double a = 7.5, b = 1.0;
    auto gamma_dist = new Gamma();
    gamma_dist->set_param_a(a);
    gamma_dist->set_param_b(b);
    auto gamma_rnd = Generator::create_random_stream(gamma_dist);
    cout << "This is a random number by **GAMMA** Distribution    : " << gamma_rnd.gen() << '\n';
    cout << endl;

  /* Gamma variable value with >> */
    double gamma_OOL;
    gamma_rnd >> gamma_OOL;
    cout << "This number is by **GAMMA** '<<' operator overloading: " << gamma_OOL << '\n';
    cout << endl;

  /* Gamma out_of_range parameter */
    double a_error = -2, b_error = 0;
    auto gamma_dist_error = new Gamma();

    cout << "Error of bad number for parameter a   : ";
    gamma_dist_error->set_param_a(a_error);

    cout << "Error of bad number for parameter b   : ";
    gamma_dist_error->set_param_b(b_error);

    auto gamma_rnd_error = Generator::create_random_stream(gamma_dist_error);
    cout << "Error of bad number trying to generate: " << gamma_rnd_error.gen();
    cout << "  <- this is value of empty double !!\n";
    cout << endl;

  /* Gaussian Distribution */
    double sigma = 5;
    auto gaussian_dist = new Gaussian();
    gaussian_dist->set_param_sigma(sigma);
    auto gaussian_rnd = Generator::create_random_stream(gaussian_dist);
    cout << "This is a random number by **Gaussian** Distribution    : " << gaussian_rnd.gen() << '\n';
    cout << endl;

  /* Gaussian variable value with >> */
    double gaussian_OOL;
    gaussian_rnd >> gaussian_OOL;
    cout << "This number is by **Gaussian** '<<' operator overloading: " << gaussian_OOL << '\n';
    cout << endl;

  /* Gaussian out_of_range parameter */
    double sigma_error = -2;
    auto gaussian_dist_error = new Gaussian();

    cout << "Error of bad number for parameter sigma   : ";
    gaussian_dist_error->set_param_sigma(a_error);

    auto gaussian_rnd_error = Generator::create_random_stream(gaussian_dist_error);
    cout << "Error of bad number trying to generate: " << gaussian_rnd_error.gen();
    cout << "  <- this is value of empty double !!\n";
    cout << endl;

  /* Poisson Distribution */
    double mu = 2;
    auto poisson_dist = new Poisson();
    poisson_dist->set_param_mu(mu);
    auto poisson_rnd = Generator::create_random_stream(poisson_dist);
    cout << "This is a random number by **Poisson** Distribution    : " << poisson_rnd.gen() << '\n';
    cout << endl;

  /* Poisson variable value with >> */
    double poisson_OOL;
    poisson_rnd >> poisson_OOL;
    cout << "This number is by **Poisson** '<<' operator overloading: " << poisson_OOL << '\n';
    cout << endl;

  /* Poisson out_of_range parameter */
    double mu_error = -2;
    auto poisson_dist_error = new Poisson();

    cout << "Error of bad number for parameter mu   : ";
    poisson_dist_error->set_param_mu(a_error);

    auto poisson_rnd_error = Generator::create_random_stream(poisson_dist_error);
    cout << "Error of bad number trying to generate: " << poisson_rnd_error.gen();
    cout << "  <- this is value of empty double !!\n";
    cout << endl;

  return 0;
}