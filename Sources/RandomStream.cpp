#include "RandomStream.hpp"

RandomStream::RandomStream (const Distribution* Dist, gsl_rng *R)   //RandomStream constructor
:dist(const_cast<Distribution*>(Dist)), r(R){                       //Get Distribution pointer (superClass pointer) and gsl_rng

}

double RandomStream::gen() {
  try
  {
    double d = dist->Calculate((const gsl_rng *)this->r);               //calculate with superClass pointer
    return d;
  }
  catch(const std::bad_alloc& e)
  {
    std::cerr << " was'nt calculated ,due to inadequate input!!!" << '\n';
  }
}


RandomStream& RandomStream::operator>>(double & d) {                //Operator >> overloading
  d = this->gen();
  return *this;                                                     //Cascading accessibility is provided :)
}