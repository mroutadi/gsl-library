
#ifndef GENERATOR_HPP
#define GENERATOR_HPP
#include <iostream>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <time.h>
#include <stdio.h>
#include <typeinfo>
#include "Distribution.hpp"
#include "RandomStream.hpp"

class Generator {
private:
  Generator();
  static Generator * Val;
  static gsl_rng *r;

public:
  static Generator* Gen();
  ~Generator();
  static RandomStream create_random_stream (const Distribution*);
  };


#endif // GENERATOR_HPP
