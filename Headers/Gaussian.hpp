
#ifndef GAUSSIAN_HPP
#define GAUSSIAN_HPP
#include "Distribution.hpp"
#include <stdexcept>

class Gaussian : public Distribution {
private:
  double param_sigma;
public:
  Gaussian();
  virtual ~Gaussian();
  void set_param_sigma(double);
  double Calculate(const gsl_rng *) override;

};


#endif // GAUSSIAN_HPP
