
#ifndef GAMMA_HPP
#define GAMMA_HPP
#include "Distribution.hpp"
#include <stdexcept>

class Gamma : public Distribution {
private:
  double param_a;
  double param_b;
public:
  Gamma();
  virtual ~Gamma();
  void set_param_a(double);
  void set_param_b(double);
  double Calculate(const gsl_rng *) override;

};


#endif // GAMMA_HPP
