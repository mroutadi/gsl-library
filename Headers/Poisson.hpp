
#ifndef POISSON_HPP
#define POISSON_HPP
#include "Distribution.hpp"
#include <stdexcept>

class Poisson : public Distribution {
private:
  double param_mu;
public:
  Poisson();
  virtual ~Poisson();
  void set_param_mu(double);
  double Calculate(const gsl_rng *) override;

};


#endif // POISSON_HPP
