
#ifndef RANDOMSTREAM_HPP
#define RANDOMSTREAM_HPP
#include <iostream>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <time.h>
#include <stdio.h>
#include <Distribution.hpp>

class RandomStream {
private:
  Distribution* dist;
  gsl_rng * r;
public:
  RandomStream(const Distribution*, gsl_rng *);
  double gen();
  RandomStream& operator>>(double &);
};


#endif // RANDOMSTREAM_HPP
