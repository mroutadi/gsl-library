
#ifndef DISTRIBUTION_HPP
#define DISTRIBUTION_HPP
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <time.h>
#include <stdio.h>

class Distribution {
private:

public:
  Distribution();
  virtual ~Distribution();
  virtual double Calculate(const gsl_rng *) = 0;
};


#endif // DISTRIBUTION_HPP
